terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "2.6.1"
    }
  }
}

# Specify vSphere provider
provider "vsphere" {
  user           = "admpcisyskube@MONEXT.NET"
  password       = "@dm!nKube2024"
  vsphere_server = "172.22.27.10"
  allow_unverified_ssl = false
}
data "vsphere_datacenter" "datacenter" {
  name = "DC1"
}

data "vsphere_datastore" "datastore" {
  name          = "K8S-HPROD"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = "cluster1-3-dev"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "k8snetwork" {
  name          = "LAN197-SVN"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_virtual_machine" "template" {
  name          = "rhel8_kube_templ"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

resource "vsphere_virtual_machine" "vm" {
  name             = "k8snode1h04"
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  num_cpus         = 4
  memory           = 16384
  network_interface {
    network_id = data.vsphere_network.k8snetwork.id
  }
#  disk {
#    label = "disk0"
#    size  = 20
#  }
  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
    customize {
      linux_options {
        host_name = "k8snode1h04"
        domain    = "dc1lan.local"
      }
      network_interface {
        ipv4_address = "172.22.197.114"
        ipv4_netmask = 24
      }
      ipv4_gateway = "172.22.197.1"
    }
  }
}

